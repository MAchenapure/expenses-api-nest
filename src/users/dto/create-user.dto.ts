export class CreateUserDto {
    readonly email: string;
    password: string;
    readonly name: string;
    readonly surname: string;
}
