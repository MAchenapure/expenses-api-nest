export const errorMessages = {
    'MongoServerError': {
        11000: 'Ya existe un usuario registrado con ese email.'
    },
    'ExpenseError': {
        'not-found': 'No existe un gasto registrado con ese id.'
    },
    'UserError': {
        'not-found': 'No existe un usuario registrado con ese id.'
    },
    'Common': {
        'internal-error': 'Ha ocurrido un error en la aplicación.'
    }
}