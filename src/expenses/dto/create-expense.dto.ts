export class CreateExpenseDto {
    readonly date: Date; 
    readonly value: number;
}